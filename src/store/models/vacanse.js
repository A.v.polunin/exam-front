export default {
    actions: {
      async fetchVacanse(ctx) {
        const res = await fetch('/api/main.json')
        const vacanse = await res.json() 
        ctx.commit("setVacanses", vacanse)
      },
    },
    mutations: {
      setVacanses (state, vacanse) {
        state.vacanses = vacanse;
      }
    },
    state: {
      vacanses:[]
    },
    getters: {
      allVacanses(state) {
        return state.vacanses
      }
    },
  
  }