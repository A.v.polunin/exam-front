import Vuex from 'vuex'
import Vue from 'vue'
import vacanse from './models/vacanse'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    vacanse
  }
})

export default store;
