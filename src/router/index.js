import VueRouter from 'vue-router'
import MainPage from '@/pages/MainPage'
import DetailsPage from '@/pages/DetailsPage'
import RegisterPage from '@/pages/RegisterPage'
import VacansesPage from '@/pages/VacansesPage'
import ErrorHandler from '@/pages/ErrorHandler'

export default new VueRouter({
    mode: "history",
    routes: [
      {
        path: "/",
        name: "main",
        component: MainPage
      },
      {
        path: "/Vacanses",
        name: "vacanses",
        component: VacansesPage
      },
      {
        path: "/Register",
        name: "register",
        component: RegisterPage
      },
      {
        path: "*",
        component: ErrorHandler
      },
      {
        path: "/Vacanses/:id",
        name: "detail",
        component: DetailsPage
      }
    ]
})